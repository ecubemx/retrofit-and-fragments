package com.lalo.retrofit_fragments.Providers

import com.lalo.retrofit_fragments.ConfiguracionInicial.ConfiguracionInicialRetrofit
import com.lalo.retrofit_fragments.ConfiguracionInicial.JsonPlaceHolder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class GlobalRetrofit {
    val URL_base=    "https://jsonplaceholder.typicode.com/"
    fun GetRetrofitConfig():JsonPlaceHolder
    {
        var mRetrofit= Retrofit.Builder()
            .baseUrl(URL_base).addConverterFactory(GsonConverterFactory.create())
            .build()
        return mRetrofit.create(JsonPlaceHolder::class.java)
    }

}