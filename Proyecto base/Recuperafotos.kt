package com.lalo.retrofit_fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.lalo.retrofit_fragments.Adaptadores.PhotosAdapter
import com.lalo.retrofit_fragments.ConfiguracionInicial.ConfiguracionInicialRetrofit.Companion.RetrofitGlobal
import com.lalo.retrofit_fragments.Models.photos
import com.lalo.retrofit_fragments.databinding.FragmentRecuperafotosBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Recuperafotos : Fragment() {
    var _binding: FragmentRecuperafotosBinding? = null
    val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        consumirfotosget()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRecuperafotosBinding.inflate(inflater)
        val view = binding.root
        // Inflate the layout for this fragment
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //TODO()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
//
fun consumirfotosget(){
    var callrespuesta =RetrofitGlobal.recuperapics()
    callrespuesta.enqueue(object : Callback<ArrayList<photos>> {
        override fun onResponse(
            call: Call<ArrayList<photos>>,
            response: Response<ArrayList<photos>>
        ) {
            if(response.isSuccessful){
                val listaRecibida=response.body()
                val adaptador = listaRecibida?.let { PhotosAdapter(activity as MainActivity,listaRecibida) }
                if(listaRecibida != null)
//                               var myDataset = listaRecibida
                // adaptador = ItemAdapter(applicationContext,listaRecibida!!)
                binding.recicler.adapter=adaptador
                binding.recicler.layoutManager= LinearLayoutManager(activity as MainActivity)
                binding.recicler.hasFixedSize()
                /*
                   listaRecibida.forEach(){
                       Log.d("mensaje","postId ${it.id}")
                   }*/
            }
            else{
                Toast.makeText(activity as MainActivity,"statuscode ${response.code()}",Toast.LENGTH_LONG)}

        }

        override fun onFailure(call: Call<ArrayList<photos>>, t: Throwable) {
            Toast.makeText(activity as MainActivity,"el servidor se cayo",Toast.LENGTH_LONG)
        }
    })
}

//

}